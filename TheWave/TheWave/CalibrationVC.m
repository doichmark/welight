//
//  CalibrationVC.m
//  TheWave
//
//  Created by Chris Meehan on 12/3/15.
//  Copyright © 2015 Christopher Meehan. All rights reserved.
//

#import "CalibrationVC.h"

@implementation CalibrationVC

- (IBAction)doneWasHit:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) removeArrowImage{
    [self.upOrDownImage setImage:nil];
}

- (IBAction)sppedUp:(id)sender {
    [self.upOrDownImage setImage:[UIImage imageNamed:@"ArrowUp.jpg"]];
    [arrowDisplayTimer invalidate];
    arrowDisplayTimer = [NSTimer scheduledTimerWithTimeInterval:(0.2) target:self selector:@selector(removeArrowImage) userInfo:nil repeats:YES];
    offset = offset + 0.05;
}



- (IBAction)slowDown:(id)sender {
    [self.upOrDownImage setImage:[UIImage imageNamed:@"ArrowDown.jpg"]];
    [arrowDisplayTimer invalidate];
    arrowDisplayTimer = [NSTimer scheduledTimerWithTimeInterval:(0.2) target:self selector:@selector(removeArrowImage) userInfo:nil repeats:YES];
    offset = offset - 0.05;
}

// This method only resets your offset back to 0.0
- (IBAction)resetApp:(id)sender {
    offset = 0.0;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    delegates = [[UIApplication sharedApplication] delegate];
    offset = delegates->masterOffset;
    weHaveCancelledTheCalib = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.slowDownButton.layer.cornerRadius = 10;
    self.slowDownButton.clipsToBounds = YES;
    
    self.doneButton.layer.cornerRadius = 10;
    self.doneButton.clipsToBounds = YES;
    
    self.resetButton.layer.cornerRadius = 10;
    self.resetButton.clipsToBounds = YES;
    
    self.speedUpButton.layer.cornerRadius = 10;
    self.speedUpButton.clipsToBounds = YES;

    [self startLoopingAndConstintlyCheckingIfTheActualTimeNeedsToBeUpdatedOnScreen];
}

// We will use the point where this VC dissapears to start saving everything.
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    delegates->masterOffset = offset;
    weHaveCancelledTheCalib = YES;
    [self.myNSOpQueue cancelAllOperations];

    // Lets never loose the offset the user set up
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(offset) forKey:@"offset"];
    [defaults synchronize];}

// Yes, this method exists in this class, and in Sender.m     We will fix that one day.
-(double) calibratedSeconds{
    return [[NSDate date] timeIntervalSince1970] + offset;
}

// This is the calidbration loop.
- (void)startLoopingAndConstintlyCheckingIfTheActualTimeNeedsToBeUpdatedOnScreen{
    lastKnownTotalSecondsNoDecimals = [self calibratedSeconds];
    //Lets set up the HUD to display these letters and progress.
    self.myNSOpQueue = [[NSOperationQueue alloc]init]; // He hit go, lets get the NSOperationQueue ready to take operations.
    [self.myNSOpQueue setMaxConcurrentOperationCount:1]; // This NSOperationQueue (aka: holder of NSOperations), shall only allow 1 thread.[self calibratedSeconds
    NSBlockOperation* symbolOperation = [NSBlockOperation new]; // Create a new operation just for this letter.
    // Beginning of block.
    [symbolOperation addExecutionBlock:^{
        while (!weHaveCancelledTheCalib){
            tempTimeHolder = (int) [self calibratedSeconds];
            if ( lastKnownTotalSecondsNoDecimals < tempTimeHolder){
                lastKnownTotalSecondsNoDecimals++;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    self.counterLabel.text = [NSString stringWithFormat:@"%d",(lastKnownTotalSecondsNoDecimals%100)];
                    [self.view setNeedsDisplay];
                }];
            }
        }
    }];
    [self.myNSOpQueue addOperation:symbolOperation];
}


@end
