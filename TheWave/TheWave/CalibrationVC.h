//
//  CalibrationVC.h
//  TheWave
//
//  Created by Chris Meehan on 12/3/15.
//  Copyright © 2015 Christopher Meehan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CalibrationVC : UIViewController{
    double offset;       // human eye frame rate is 1/16 i think. So lets go 1/20, per click.
    AppDelegate* delegates;
    int lastKnownTotalSecondsNoDecimals;
    BOOL weHaveCancelledTheCalib;
    int tempTimeHolder;
    NSTimer* arrowDisplayTimer;
}
@property (weak, nonatomic) IBOutlet UIButton *slowDownButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (weak, nonatomic) IBOutlet UIButton *speedUpButton;
@property (weak, nonatomic) IBOutlet UIImageView *upOrDownImage;
@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property (strong,nonatomic) NSOperationQueue *myNSOpQueue; // This will basically hold an array of operations to perform on another queue.

@end
