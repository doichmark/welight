//
//  AppDelegate.h
//  TheWave
//
//  Created by Chris Meehan on 11/28/15.
//  Copyright © 2015 Christopher Meehan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    @public
    double masterOffset;  // This will hold the master copy of the offset for the whole app.
    int myNumber;
    int outOfHowMany;
    int patternNumber;
    BOOL doesItLoop; // Is this sequence supposed to loop?
}

@property (strong, nonatomic) UIWindow *window;

@end

