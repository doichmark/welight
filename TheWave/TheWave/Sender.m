//
//  CAMViewController.m
//  MorseCode
//
//  Created by Chris Meehan on 1/20/14.
//  Copyright (c) 2014 Chris Meehan. All rights reserved.
//

#import "Sender.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h> // Foundation for our camera flasher.

// All properties should be @property
@interface Sender (){
    float timeMagnitude; // Only use CGFloat, and int's should be NSInteger, see comments in your string category.
    // Also this should be a property, no "raw values"
    NSDate* exactStartTime;
    int firstSeenSeconds;
    double offset;       // human eye frame rate is 1/16 i think. So lets go 1/20, per click.
    AppDelegate* delegates;
    float frameRateOfASecond;
    BOOL isTheLightOn;
    int numOfMoves;
    int numOfPhones;
    int myNum;
    double nextUpMarkerInSeconds;
    BOOL areWeSupposedToBeFlashing;
    BOOL canThisDeviceFlash;
    AVCaptureDevice *device;
}

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *leftAddButton;
@property (weak, nonatomic) IBOutlet UIButton *rightAddButton;
@property (weak, nonatomic) IBOutlet UIButton *decreaseMyDeviceButton;
@property (weak, nonatomic) IBOutlet UIButton *increaseMyDeviceButton;
@property (weak, nonatomic) IBOutlet UIButton *decreaseTotalPhonesButton;
@property (weak, nonatomic) IBOutlet UIButton *increaseTotalPhonesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCalibrationButton;



@property (weak, nonatomic) IBOutlet UILabel *startFlashingMessage;
@property (weak, nonatomic) IBOutlet UILabel *startFlashingLabel;

@property (weak, nonatomic) IBOutlet UILabel *myDeviceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalNumOfDevicesLabel;
@property (weak, nonatomic) IBOutlet UIButton *startOrStopFlashingButton;


@property (weak, nonatomic) IBOutlet UILabel *SecondsLabel;
@property (weak, nonatomic) IBOutlet UIButton *myUIActivateButton; // This is the only button.
@property (strong,nonatomic) NSArray* arrayOfOnAnOffBooleans;
@property (strong,nonatomic) NSOperationQueue *myNSOpQueue; // This will basically hold an array of operations to perform on another queue.
@end

@implementation Sender

- (IBAction)button1WasHit:(id)sender {
    [self enableOnlyButton:0];
    delegates->patternNumber = 0;
    [self stopTheFlashing];
}

- (IBAction)button2WasHit:(id)sender {
    [self enableOnlyButton:1];
    delegates->patternNumber = 1;
    [self stopTheFlashing];
}

- (IBAction)button3WasHit:(id)sender {
    [self enableOnlyButton:2];
    delegates->patternNumber = 2;
    [self stopTheFlashing];
}



- (IBAction)startOrStopFlashingWasHit:(id)sender {
    // If this button was hit while not flashing, then lets start flashing.
    if(!areWeSupposedToBeFlashing){
        [self.startOrStopFlashingButton setTitle:@"Stop Flashing" forState:UIControlStateNormal];
        [self.startOrStopFlashingButton setBackgroundColor:[UIColor redColor]];
        [self.startOrStopFlashingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.startFlashingMessage.hidden = YES;
        self.startFlashingLabel.hidden = YES;
        areWeSupposedToBeFlashing = YES;
        [self loadThisPhonesBooleanArray]; // Get the boolean pattern ready for this phone only.
        if (canThisDeviceFlash) {
            [self keepLoopingToSeeIfYouNeedToChangeTheLight];
        }
    }
    else{   // Otherwise, if this button was hit while flashing, then let's stop the flashing.
        [self stopTheFlashing];
    }
}

- (IBAction)decreaseMyDevicePosition{
    myNum = myNum - 1;
    if ( myNum < 1 ) { // We can never be less than 1.
        myNum = 1;
    }
    delegates->myNumber = myNum; // Tell the master too.
    self.myDeviceNumberLabel.text = [NSString stringWithFormat:@"%d",myNum];
    [self stopTheFlashing];
}

- (IBAction)increaseMyDevicePosition{
    myNum = myNum + 1;
    if (numOfPhones < myNum) {
        myNum = numOfPhones;  // You can not have a higher number than the number of devices.
    }
    delegates->myNumber = myNum; // Tell the master too.
    self.myDeviceNumberLabel.text = [NSString stringWithFormat:@"%d",myNum];
    [self stopTheFlashing];
}

- (IBAction)decreaseTotalNumOfDevices{
    numOfPhones = numOfPhones - 1;
    if ( numOfPhones < 2 ) { // We should never allow less than 2 devices, otherwise what's the point.
        numOfPhones = 2;
    }
    if (numOfPhones < myNum) {
        numOfPhones = myNum;  // You can not have a smaller number of phones, than your phone's number.
    }
    delegates->outOfHowMany = numOfPhones; // Tell the master too.
    self.totalNumOfDevicesLabel.text = [NSString stringWithFormat:@"%d",numOfPhones];
    [self stopTheFlashing];
}

- (IBAction)increaseTotalNumOfDevices{
    numOfPhones = numOfPhones + 1;
    delegates->outOfHowMany = numOfPhones; // Tell the master too.
    self.totalNumOfDevicesLabel.text = [NSString stringWithFormat:@"%d",numOfPhones];
    [self stopTheFlashing];
}

- (IBAction)addSomeoneToTheLeft:(id)sender {
    [self increaseMyDevicePosition];
    [self increaseTotalNumOfDevices];
}
- (IBAction)removeSomeoneFromTheLeft:(id)sender {
    [self decreaseMyDevicePosition];
    [self decreaseTotalNumOfDevices];
}
- (IBAction)addSomeoneToTheRight:(id)sender {
    [self increaseTotalNumOfDevices];
}
- (IBAction)removeSomeoneFromTheRight:(id)sender {
    [self decreaseTotalNumOfDevices];
}


// This method only counts down till the end of a 10 second mark, then turns on only. Obsolete now.
- (IBAction)startFlashing:(id)sender {
    sleep(1);    // I am re-initializing the OperationQue,    i wanna make sure the old one finishes.
    //Lets set up the HUD to display these letters and progress.
    self.myNSOpQueue = [[NSOperationQueue alloc]init]; // He hit go, lets get the NSOperationQueue ready to take operations.
    [self.myNSOpQueue setMaxConcurrentOperationCount:1]; // This NSOperationQueue (aka: holder of NSOperations), shall only allow 1 thread.
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice"); // Get the class of whatever av hardware this device has.
    if (captureDeviceClass != nil) { // As long as this device has some av device.
        if (canThisDeviceFlash){ // If it can support flash.
            int removedMilliseconds = (int) [self calibratedSeconds];
            // Now find out the seconds
            removedMilliseconds = removedMilliseconds - ( removedMilliseconds % 10 ); // This puts us into a 10's bracket
            double removedMillisecondsFloat = (double)removedMilliseconds + 10.0; // And this tops us, so it's for sure the future.
            while ([self calibratedSeconds] < removedMillisecondsFloat) {
                // We will just keep looping this until its start time.
            }
            //    It's go time!   Turn the light on and start!
            [self turnTorchOn];
        }
    }
}

-(double) calibratedSeconds{
    return [[NSDate date] timeIntervalSince1970] + offset;
}

// We will use the point where this VC dissapears to start saving the positions.
-(void)viewWillDisappear:(BOOL)animated{ // When we leave this screen in any way, we stop any currently running queue.
    [super viewWillDisappear:animated];
    [self stopTheFlashing];
    
    // Lets never loose the positions the user set up
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(myNum) forKey:@"deviceNum"];
    [defaults setObject:@(delegates->patternNumber) forKey:@"patternNumber"];
    [defaults setObject:@(numOfPhones) forKey:@"totalDevices"];
    [defaults synchronize];
}

// This only gets called once per session.
- (void)viewDidLoad{
    [super viewDidLoad];
    delegates = [[UIApplication sharedApplication] delegate];
    isTheLightOn = NO;
    frameRateOfASecond = 0.1;  // Each frame will be 0.1 seconds long.
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo]; // Grab the camera device.
    if ([device hasTorch] && [device hasFlash]){ // If it can support flash.
        canThisDeviceFlash = YES;
    }
    else{
        canThisDeviceFlash = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    numOfPhones = delegates->outOfHowMany;
    myNum = delegates->myNumber;
    self.myDeviceNumberLabel.text = [NSString stringWithFormat:@"%d",myNum];
    self.totalNumOfDevicesLabel.text = [NSString stringWithFormat:@"%d",numOfPhones];
    offset = delegates->masterOffset;
    [self.startOrStopFlashingButton setTitle:@"Start Flashing" forState:UIControlStateNormal];
    [self.startOrStopFlashingButton setBackgroundColor:[UIColor greenColor]];
    [self.startOrStopFlashingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.startFlashingMessage.hidden = NO;
    self.startFlashingLabel.hidden = NO;
    areWeSupposedToBeFlashing = NO;
    [self enableOnlyButton:delegates->patternNumber];
    
    self.startOrStopFlashingButton.layer.cornerRadius = 10; 
    self.startOrStopFlashingButton.clipsToBounds = YES; 
    
    self.leftAddButton.layer.cornerRadius = 10; 
    self.leftAddButton.clipsToBounds = YES; 
    
    self.rightAddButton.layer.cornerRadius = 10; 
    self.rightAddButton.clipsToBounds = YES; 
    
    self.decreaseMyDeviceButton.layer.cornerRadius = 10; 
    self.decreaseMyDeviceButton.clipsToBounds = YES; 
    
    self.increaseMyDeviceButton.layer.cornerRadius = 10; 
    self.increaseMyDeviceButton.clipsToBounds = YES; 
    
    self.decreaseTotalPhonesButton.layer.cornerRadius = 10; 
    self.decreaseTotalPhonesButton.clipsToBounds = YES; 
    
    self.increaseTotalPhonesButton.layer.cornerRadius = 10; 
    self.increaseTotalPhonesButton.clipsToBounds = YES; 
    
    self.viewCalibrationButton.layer.cornerRadius = 10; 
    self.viewCalibrationButton.clipsToBounds = YES; 
}

-(void)enableOnlyButton:(int)buttonNum{
    if (buttonNum==0) {
        self.button1.backgroundColor = [UIColor cyanColor];
        self.button2.backgroundColor = [UIColor lightGrayColor];
        self.button3.backgroundColor = [UIColor lightGrayColor];
        
    }
    else if (buttonNum == 1){
        self.button1.backgroundColor = [UIColor lightGrayColor];
        self.button2.backgroundColor = [UIColor cyanColor];
        self.button3.backgroundColor = [UIColor lightGrayColor];
    }
    else{
        self.button1.backgroundColor = [UIColor lightGrayColor];
        self.button2.backgroundColor = [UIColor lightGrayColor];
        self.button3.backgroundColor = [UIColor cyanColor];
    }
}

// In this method, runs all the logic that makes your device's light decide if it should turn on or not.
-(void) keepLoopingToSeeIfYouNeedToChangeTheLight{
    //Lets set up the HUD to display these letters and progress.
    self.myNSOpQueue = [[NSOperationQueue alloc]init]; // He hit go, lets get the NSOperationQueue ready to take operations.
    [self.myNSOpQueue setMaxConcurrentOperationCount:1]; // This NSOperationQueue (aka: holder of NSOperations), shall only allow 1 thread.[self calibratedSeconds
    NSBlockOperation* symbolOperation = [NSBlockOperation new]; // Create a new operation just for this letter.
    // Beginning of block.
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice"); // Get the class of whatever av hardware this device has.
    if (captureDeviceClass != nil) { // As long as this device has some av device.
        [symbolOperation addExecutionBlock:^{
            // This needs to be at the next time marker coming up, that starts a loop.
            // Also these marker numbers should be the same across all devices......not calibrated values.
            // And lets no choose the reset marker coming up, lets choose the one after that, incase it happens too fast.
            nextUpMarkerInSeconds = [self returnTheMarkerOfTheResetLoopAfterTheNext];
            int currentBooleanArrayIndex = 0;
            NSNumber* tempBool;
            //    ..... and GO!
            while (areWeSupposedToBeFlashing) {
                if (nextUpMarkerInSeconds <= [self calibratedSeconds]){ // We have just hit or passed another marker. Let's see if we need to change.
                    
                    currentBooleanArrayIndex++;
                    if (currentBooleanArrayIndex == numOfMoves) {
                        currentBooleanArrayIndex = 0; // Reset this back to zero once we reached our last step.
                    }
                    
                    tempBool = self.arrayOfOnAnOffBooleans[currentBooleanArrayIndex];
                    
                    // If the light is currently on and needs to turn off.
                    if(isTheLightOn && tempBool == [NSNumber numberWithInt:0]) {
                        [self turnOffTheTorch];
                    }
                    else if (!isTheLightOn && tempBool == [NSNumber numberWithInt:1]){ // Otherwise, if the light is off and needs to be turned on.
                        [self turnTorchOn];
                    }
                    // No matter what happended, we did just hit a marker, so let do diligance.
                    nextUpMarkerInSeconds = nextUpMarkerInSeconds + 0.1;  // As long as we are alwayse a 1/10 second frame rate, then this will stay true.
                }
            }
        }];
        [self.myNSOpQueue addOperation:symbolOperation];
    }
}

// This returns the exact time (in seconds), that will be the beginning of the next loop to flash out.
-(double)returnTheMarkerOfTheResetLoopAfterTheNext{
    double tempTime = [self calibratedSeconds];
    double temp = tempTime * 10; // This is so we can get the decimal too
    int64_t anotherTemp = temp;
    int temp2 = anotherTemp % (numOfMoves); // This should be how many 0.1 we are past the last loop restart marker.
    int howManyDecimalsTillNextMove = (numOfMoves) - temp2;
    double test = ( (double) (anotherTemp + (numOfMoves) + howManyDecimalsTillNextMove) / 10.0 );
    
    
    return test;
}

// This should get called, whenever someone clicks "Start Flashing", so that means the flasher should turn off whenever any change has been made
- (void)loadThisPhonesBooleanArray{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    if (delegates->patternNumber == 0){  // This is "show 1"
        numOfMoves = ( ( numOfPhones*2 ) -2 ); // I know ahead of time that this is how many moves this will take.
        if (myNum == 1) {   // If this is the first phone.
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == 0){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }
        else if(myNum == numOfPhones){   // or if this is the last phone.
            int theCenterishIndexNum = ( numOfMoves/2 );
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == theCenterishIndexNum){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }
        else {  // If this is any phone but the 1st or last...
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == myNum-1){
                    [array addObject:@(1)];
                }
                else if (count == numOfMoves-myNum+1){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }
    }
    else if (delegates->patternNumber == 1){ // This is "show 2"
        numOfMoves = 3; // This will be like a long string of moving lights (that are 1/3 wide each)
        if (myNum%3 == 0) {   // If this is the 3rd, 6th, 9th, etc..... It will start with light on.
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == 0){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }
        else if(myNum%3 == 1){ // If this is the 1st, 4th, 7th, 10th, etc.....
            int theCenterishIndexNum = ( numOfMoves/2 );
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == theCenterishIndexNum){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }
        else {  // Otherwise, this must be the 2nd, 5th, 8th, 11th, etc......
            for (int count = 0 ; count<numOfMoves ; count++  ){
                if (count == myNum-1){
                    [array addObject:@(1)];
                }
                else if (count == numOfMoves-myNum+1){
                    [array addObject:@(1)];
                }
                else {
                    [array addObject:@(0)];
                }
            }
        }

    }
    else{ // This must be the last one, the "signal" pattern.
        numOfMoves = 10; // I know ahead of time that this is how many moves this will take.

        // Then, no matter what, lets do a syncronized flash sequence. This will end up do
        for (int count = 0 ; count<5 ; count++  ){ // 5 should be 0.5 seconds of no light
            [array addObject:@(0)];
        }
        for (int count = 0 ; count<5 ; count++  ){ // 5 should be 0.5 seconds of light
            [array addObject:@(1)];
        }
    }
    self.arrayOfOnAnOffBooleans = array;
}



// This should shut down all flashing background threads running.
-(void) stopTheFlashing{
    if (canThisDeviceFlash){ // If it can support flash.
        [self.myNSOpQueue cancelAllOperations];
        areWeSupposedToBeFlashing = NO;
        [self.startOrStopFlashingButton setTitle:@"Start Flashing" forState:UIControlStateNormal];
        [self.startOrStopFlashingButton setBackgroundColor:[UIColor greenColor]];
        [self.startOrStopFlashingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.startFlashingMessage.hidden = NO;
        self.startFlashingLabel.hidden = NO;
        [self turnOffTheTorch];
    }
}

- (void)turnTorchOn{
    [device lockForConfiguration:nil]; // This gets exclusive access to the camera's torch.
    [device setTorchMode:AVCaptureTorchModeOn]; //These two lines turn the light on.
    [device setFlashMode:AVCaptureFlashModeOn];
    [device unlockForConfiguration];
    isTheLightOn = YES;
}

-(void)turnOffTheTorch{
    [device lockForConfiguration:nil]; // This gets exclusive access to the camera's torch.
    [device setTorchMode:AVCaptureTorchModeOff];
    [device setFlashMode:AVCaptureFlashModeOff];
    [device unlockForConfiguration];
    isTheLightOn = NO;
}

@end
